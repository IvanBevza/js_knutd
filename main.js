$(document).ready(function() {
    $('#submit').on('click', function(e) {
        e.preventDefault();

        let name = 'anonymous' || $('#name').val();
        let group = $('#group').val();
        let sex = $("input[name='sex']:checked").val();
        let education = $("input[name='education']:checked").val();

        let result = $('#result').html(        
        `<div class='mt-3'>
            <h2>Student data:</h2>
            <p>Name - ${name}</p>
            <p>Group - ${group}</p>
            <p>Sex - ${sex}</p>
            <p>Education plan - ${education}</p>
        </div>`);
    });
});
