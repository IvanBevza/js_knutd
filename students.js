$(document).ready(function() {
    $('#calculate').on('click', function(e) {
        e.preventDefault();

        let total =$('#total').val();
        let present = $('#present').val();

        console.log(total);
        let results = $('#results').html(        
        `<div class='mt-3'>
            <h2>Ratio:</h2>
            <p>Total - ${total}</p>
            <p>Present - ${present}</p>
            <p>Ratio - ${(present / total * 100).toFixed(2)}%</p>
        </div>`);
    });
});
